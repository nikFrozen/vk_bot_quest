#Счетчики для изменения диалогов
REP=int(0)
girl=int(0)
stone=int(0)
bilvcentre=int(0)
bilvsevere=int(0)
sever=int(0)
bilvyuge=int(0)
bilvozere=int(0)
babka=int(0)
babkaend=int(0)
ozero=int(0)
rusalka=int(0)
platok=int(0)
nekr=int(0)

#Функция для отправления сообщения юзеру
def write_msg(user_id, random_id, message): 
    vk_session.method('messages.send', {'user_id': user_id, "random_id":
random_id, 'message': message})


def start(): #Начало начал
    for event in longpoll.listen():
        if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text:
            if event.from_user:
                request=event.text.casefold() #Дальше идет обнуление переменных, на тот случай, если квест заново начнется без перезагрузки бота
                global REP
                global girl
                global stone
                global bilvcentre
                global bilvsevere
                global sever
                global bilvyuge
                global bilvozere
                global babka
                global babkaend
                global ozero
                global rusalka
                global platok
                global nekr 
                if request == "начать":
                    write_msg(event.user_id, event.random_id, "Этой ночью сгорела вся ваша деревня. Чтобы хоть как-то жить "
                                                              "нужно (идти) дальше через лес и добраться до другой "
                                                              "деревни. Там живет ваш дядя Джуш. Вы собрали все  "
                                                              "уцелевшие вещи и собрались в путь. Надо бы (осмотреться) в"
                                                              " последний раз... ")
                    REP = 0
                    girl = 0
                    stone =0
                    bilvcentre = 0
                    bilvsevere =0
                    sever = 0
                    bilvyuge = 0
                    bilvozere = 0
                    babka = 0
                    babkaend = 0
                    ozero = 0
                    rusalka = 0
                    platok = 0
                    nekr = 0
                    nachalo()
                else:
                    write_msg(event.user_id, event.random_id, "Напишите (Начать).")


def nachalo(): # Начало квеста
    for event in longpoll.listen():
        if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text:
            if event.from_user:
                request=event.text.casefold()
                if request == "идти":
                    write_msg(event.user_id, event.random_id, "Вы вошли в лес. Благо тут есть тропинка, главное не сворачивать "
                                                              "с нее и (идти) дальше. Недалеко от тропинки лежит человек."
                                                              "Надо бы (осмотреться). Может его (разбудить)? Или пока "
                                                              "никого нет рядом, можно попробовать его (ограбить).")
                    les()
                elif request == "осмотреться":
                    write_msg(event.user_id, event.random_id, "Вы посмотрели в последний раз на то, что осталось от деревни " 
                                                              "в которой вы провели всю свою жизнь... ")
                else:
                    write_msg(event.user_id, event.random_id, "Доступные действия (идти), (осмотреться).")

def les(): #Лес
    for event in longpoll.listen():
        if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text:
            if event.from_user:
                request=event.text.casefold()
                global REP
                if request == "ограбить":
                    write_msg(event.user_id, event.random_id, "Вы попытались обобрать незнакомого человека, но только вы начали " 
                                                              "шарить по корманам как он проснулся. Он резко встал, ударил вас и "
                                                              "убежал в глубь леса.")
                    REP-=1
                    write_msg(event.user_id, event.random_id, "Ваше здоровье - 10, репутация -1.")
                    write_msg(event.user_id, event.random_id, "Вы отправились дальше в глубь леса. "
                                                              "Через несколько часов вы дошли до дяди. Он увидел вас и п"
                                                              "озвал подойти, самое время (поздороваться).")
                    uncle()

                elif request == "разбудить":
                    write_msg(event.user_id, event.random_id, "Вы разбудили этого человека. Вчера он перебрал и уснул прямо " 
                                                              " на обочине. Если бы не вы, какой-нибудь негодяй мог его ограбить. "
                                                              "Он вас поблагодарил и пошел своей дорогой. ")
                    REP+=1
                    write_msg(event.user_id, event.random_id, "Репутация +1.")
                    write_msg(event.user_id, event.random_id, "Вы отправились дальше в глубь леса. "
                                                              "Через несколько часов вы дошли до дяди. Он увидел вас и п"
                                                              "озвал подойти, самое время (поздороваться).")
                    uncle()
                elif request == "осмотреться":
                    write_msg(event.user_id, event.random_id, "Вы подошли к лежащему человеку. Похоже какой-то пьяница. Даже "
                                                              "не знаю, стоит ли его трогать... ")
                elif request == "идти":
                    write_msg(event.user_id, event.random_id, "Вы прошли мимо человека и отправились дальше в глубь леса. ")
                    write_msg(event.user_id, event.random_id, "Через несколько часов вы дошли до дяди. Он увидел вас и п"
                                                              "озвал подойти, самое время (поздороваться).")
                    uncle()
                else:
                    write_msg(event.user_id, event.random_id, "Доступные действия (осмотреться), (разбудить), (ограбить), (идти). ")

def uncle(): #Дядя
    for event in longpoll.listen():
        if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text:
            if event.from_user:
                request=event.text.casefold()
                global REP
                if request == "поздороваться" and REP==1:
                    write_msg(event.user_id, event.random_id, "Ну здравствую племяш дорогой. Пошли скорее домой я тебя накормлю! ")
                    write_msg(event.user_id, event.random_id, "Вы переночевали у дяди. На следующее утро после завтрака вы решили "
                                                              "отправится в (центр) деревни, посмотреть что там да как. ")

                    center()
                elif request == "поздороваться" and REP==0:
                    write_msg(event.user_id, event.random_id, "Привет. Слышал про твою беду. Ладно, оставайся на некоторое время. "
                                                              "Но знай что у меня лодырей в доме нет, с утра и до вечера будем "
                                                              "работать. ")
                    write_msg(event.user_id, event.random_id, "Вы переночевали у дяди. На следующее утро после завтрака вы решили "
                                                              "отправится в (центр) деревни, посмотреть что там да как. ")
                    center()
                elif request == "поздороваться" and REP ==-1:
                    write_msg(event.user_id, event.random_id, "Хрену тебе, а не привет. А ну пошел отсюда, еще не хватало мне "
                                                              "воришек в доме держать! ")
                    write_msg(event.user_id, event.random_id, "Дядя не пустил вас домой, пришлось ночевать в каком-то забро"
                                                              "шенном сарае с дырявой крышей и разбитыми окнами. Вы голодали "
                                                              "и мерзли. ")

                    write_msg(event.user_id, event.random_id, "Ваше здоровье - 25.")
                    write_msg(event.user_id, event.random_id, "На следующее утро вы решили отправится в (центр) "
                                                              "деревни, посмотреть что там да как. ")

                    center()

                elif request =="осмотреться":
                    write_msg(event.user_id, event.random_id, "Вы добрались до дома дяди Джуша. А вот и он, самое время "
                                                              "(поздороваться) с ним. ")
                else:
                    write_msg(event.user_id, event.random_id, "Доступные действия (осмотреться), (поздороваться).")


def center(): #Центр деревни
    for event in longpoll.listen():
        if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text:
            if event.from_user:
                request=event.text.casefold()
                global bilvcentre
                if request =="центр" and bilvcentre == 0:
                    write_msg(event.user_id, event.random_id, "Вы пришли в (центр) деревни. По дороге мальчуган "
                                                                  "рассказал куда здесь можно сходить. (север) дом бабули, "
                                                                  "а если пойдете еще дальше на (север), то дойдете до озера, "
                                                                  "(запад) заброшенный дом, место где собирается все отребье, "
                                                                  " бандиты, воры и т.д., (юг) кладбище, (восток) дом старей"
                                                                  "шины деревни. Хм, куда бы отправится...")
                    bilvcentre+=1
                    vyborizcentra ()
                else:
                    write_msg(event.user_id, event.random_id, "Доступные действия (центр).")


def vyborizcentra (): #Выбор из центра
    for event in longpoll.listen():
        if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text:
            if event.from_user:
                request=event.text.casefold()
                global bilvsevere
                if request == "север" and bilvsevere==0:
                    write_msg(event.user_id, event.random_id, "Вы находитесь у дома бабушки Зельды. Она расстроенная с"
                                                                  "идит перед домом. Наверное стоит (подойти) поговорить"
                                                              " или идти дальше своей дорогой на (север) или вернуться в (центр).")
                    bilvsevere += 1
                    vyborizsevera()

                elif request == "север" and bilvsevere > 0:
                    write_msg(event.user_id, event.random_id, "Вы находитесь рядом с домом бабушки Зельды.")
                    vyborizsevera()

                elif request == "запад":
                    write_msg(event.user_id, event.random_id, "Вы дошли до заброшенного дома. Вокруг него собрались бандиты."
                                                                  "Вам тут делать нечего, вы вернулись в центр.")
                    vyborizcentra()

                elif request == "юг":
                    write_msg(event.user_id, event.random_id, "Вы пришли на кладбище.")
                    vyborizkladbishe()
                elif request == "восток":
                    write_msg(event.user_id, event.random_id, "Вы подошли к хижине старейшины, но вас не пустили внутрь."
                                                                  "Вы вернулись назад в центр.")
                    vyborizcentra()
                else:
                    write_msg(event.user_id, event.random_id, "Доступные действия (север), (восток), (юг), (запад).")

def grandma(): #Север 1. Дом бабушки
    for event in longpoll.listen():
        if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text:
            if event.from_user:
                request=event.text.casefold()
                global girl
                global REP
                global babka
                global babkaend
                global platok
                if request == "поговорить" and babka==0:
                    write_msg(event.user_id, event.random_id, "Вы поздаровались с бабушкой. Она рассказала что ее внучка "
                                                              "пошла на озеро и до сих пор не вернулась. Зельда просит "
                                                              "вас дойти до озера, которое находится дальше на (севере)"
                                                              " и увести внучку домой. Она дала вам свой платок, так внучка"
                                                              " сразу вам поверит. Вы отошли от дома. ")
                    platok+=1
                    babka+=1
                    vyborizsevera()

                elif request == "поговорить" and babkaend == 1:
                    write_msg(event.user_id, event.random_id, "Бабушка здоровается с вами и говорит что ее внучка опять"
                                                              " убежала к озеру. Но сейчас ей полегчало и она сама мо"
                                                              "жет за ней сходить. Вы отошли от дома. ")
                    vyborizsevera()
                elif request == "поговорить" and babka>0 and girl == 0:
                    write_msg(event.user_id, event.random_id, "Бабушка просит вас поторопится, она сильно переживает"
                                                              " за свою внучку. Вы отошли от дома. ")
                    vyborizsevera()
                elif request == "поговорить" and babka>0 and girl ==1:
                    write_msg(event.user_id, event.random_id, "Зельда благодарит вас за то, что вернули ее внучку домой"
                                                              " целой и невредимой. Она подарила вам свой красивый платок,"
                                                              " может он когда-нибудь вам пригодится. Репутация +1."
                                                              " Вы отошли от дома. ")
                    platok -=1
                    girl-=1
                    babkaend+=1
                    vyborizsevera()

                elif request == "север":
                    write_msg(event.user_id, event.random_id, "Вы находитесь между озером и центром.")
                    vyborizsevera()
                elif request == "центр":
                    write_msg(event.user_id, event.random_id, "Вы пришли в центр деревни")
                    vyborizcentra()

                else:
                    write_msg(event.user_id, event.random_id, "Доступные действия (поговорить), (север), (центр).")

def vyborizsevera(): #Выбор перемещения на севере 1
    for event in longpoll.listen():
        if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text:
            if event.from_user:
                request=event.text.casefold()
                global sever
                global girl
                if request == "центр":
                    write_msg(event.user_id, event.random_id, "Вы пришли в центр деревни.")
                    vyborizcentra()
                elif request == "север" and sever==0:
                    write_msg(event.user_id, event.random_id, "Вы пришли к озеру, надо бы (осмотреться). ")
                    sever+=1
                    vyborizozera()
                elif request == "север" and sever>0:
                    write_msg(event.user_id, event.random_id, "Вы пришли к озеру.")
                    vyborizozera()
                elif request =="подойти" and girl == 0:
                    write_msg(event.user_id, event.random_id, "Вы подошли к дому бабушки, она хочет с вами (поговорить).")
                    grandma()
                elif request =="подойти" and girl == 1:
                    write_msg(event.user_id, event.random_id, "Вы с девочкой подошли к дому Зельды.")
                    grandma()
                else:
                    write_msg(event.user_id, event.random_id, "Доступные действия (подойти), (север), (центр).")

def vyborizozera():# Север 2. Выбор перемещения
    for event in longpoll.listen():
        if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text:
            if event.from_user:
                request = event.text.casefold()
                global girl
                global stone
                global ozero
                global babka
                global rusalka
                global platok
                if request == "подойти" and rusalka==0:
                    write_msg(event.user_id, event.random_id, "Прекрасная русалка рассказала что кто-то из деревни украл"
                                                              " ее волшебный камень и что никто из смертных недолжен владеть такой силой. "
                                                              "Она чувствует что он находится где-то"
                                                              " рядом с усопшими людьми. Она пообещала что станет вашей "
                                                              "морской женой и заберет вас жить в море где исполнит все "
                                                              "ваши желания, если вы вернете ей ее камень.")
                    rusalka+=1
                    vyborizozera()
                elif request =="подойти" and rusalka==1 and stone==0:
                    write_msg(event.user_id, event.random_id, "Русалка просит вас не медлить. Чем дольше камень находится"
                                                              " в людских руках, тем больше бед может случится!")
                    vyborizozera()
                elif request == "камень" and stone==0:
                    write_msg(event.user_id, event.random_id, "У вас нет камня.")
                elif request == "камень" and stone==1:
                    write_msg(event.user_id, event.random_id, "Русалка благодарит вас за возвращение волшебного камня и "
                                                              "начинает читать заклинание на непонятном языке. Когда вы "
                                                              "зашли в воду, ваши ноги начали срастаться, через минуту "
                                                              "вместо них у вас появился большой хвост. Вы слились с "
                                                              "русалкой в быстром безумном подводном танце и уплыли в "
                                                              "морское царство где жили долго и счастливо.")
                    write_msg(event.user_id, event.random_id, " /GAME OVER\ ")
                    write_msg(event.user_id, event.random_id, "Напишите (начать) чтобы пройти квест заного.")
                    stone-=1
                    start()
                elif request == "увести" and platok == 0:
                    write_msg(event.user_id, event.random_id, "Вы подозвали девочку и сказали что уведете ее. Она сказала"
                                                              " что ей нужно доказать что бабушка на самом деле ее зовет."
                                                              " Например личная вещь.")
                elif request == "увести" and platok==1:
                    write_msg(event.user_id, event.random_id, "Вы рассказали девочке что бабушка переживает за нее "
                                                              "и попросила вас увести ее домой. Вы показали ей платок Зельды."
                                                              " Она согласилась пойти за вами. ")
                    write_msg(event.user_id, event.random_id, "Вы находитесь рядом с домом Зельды.")
                    girl+=1
                    vyborizsevera()
                elif request == "увести" and girl==1:
                    write_msg(event.user_id, event.random_id, "Девочка идет следом за вами.")
                    vyborizsevera()

                elif request == "уйти" and girl ==1:
                    write_msg(event.user_id, event.random_id, "Вы ушли от озера в сторону центра. Сейчас вы находитесь"
                                                              " около дома бабушки. Надо бы (подойти), а то бабушка "
                                                              "наверное уже заждалась.")
                    vyborizsevera()
                elif request == "уйти" and girl ==0:
                    write_msg(event.user_id, event.random_id, "Вы ушли от озера в сторону центра. Сейчас вы находитесь"
                                                              " около дома бабушки. Между озером на (севере) и (центром)"
                                                              " деревни.")
                    vyborizsevera()

                elif request == "осмотреться" and ozero==0 and babka >0:
                    write_msg(event.user_id, event.random_id, "У берега на корточках сидит девочка, наверное та самая,"
                                                              " которую бабушка просила (увести) домой. Она раз"
                                                          "говаривает с русалкой. Вы не поверили своим глазам, но это "
                                                          "и вправду самая настоящая русалка с хвостом вместо ног. Нико"
                                                          "го красивее вы никогда не видели. Как только она вас увидел"
                                                          "а сразу попросила (подойти), видимо ей нужна ваша помощь.  ")
                    ozero+=1
                elif request == "осмотреться" and ozero >0 and girl>0:
                    write_msg(event.user_id, event.random_id, "На берегу озера сидит русалка.")
                elif request =="осмотреться" and ozero>0:
                    write_msg(event.user_id, event.random_id, "У озера девочка играет с русалкой.")

                elif request == "осмотреться" and babka==0 and ozero==0:
                    write_msg(event.user_id, event.random_id, "У берега на корточках сидит девочка. Она "
                                                          "разговаривает с русалкой. Вы не поверили своим глазам, но это "
                                                          "и вправду самая настоящая русалка с хвостом вместо ног. Нико"
                                                          "го красивее вы никогда не видели. Как только она вас увидел"
                                                          "а сразу попросила (подойти), видимо ей нужна ваша помощь.  ")
                    ozero+=1

                else:
                    write_msg(event.user_id, event.random_id, "Доступные действия (осмотреться),(подойти), (увести),"
                                                              "(уйти), (камень).")

def vyborizkladbishe(): #Выбор из локации Кладбище.
    for event in longpoll.listen():
        if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text:
            if event.from_user:
                request = event.text.casefold()
                global stone
                global girl
                global nekr
                if request == "центр":
                    write_msg(event.user_id, event.random_id, " Вы вернулись в центр деревни.")
                    vyborizcentra()

                elif request == "осмотреться":
                    write_msg(event.user_id, event.random_id, "Как только вы пришли, за вами очень пристально начал наб"
                                                              "людать какой-то странный дядька в балахоне.")
                elif request == "осмотреться" and nekr ==1:
                    write_msg(event.user_id, event.random_id, "На кладбище как всегда тихо. Валакас стоит у могилы недал"
                                                              "еко от вас.")


                elif request == "поговорить" and nekr==0:
                    write_msg(event.user_id, event.random_id, "Странный дядька сказал что вчера ночью ему приснилось "
                                                              "будущее. Он видел как толпы мертвецов ходят по земле и "
                                                              "ничто не может их остановить. Для того чтобы это будуще"
                                                              "е стало настоящим, нужно совершить ритуал. Вам нужно п"
                                                              "ривести невинное дитя на кладбище и в полночь все нач"
                                                              "нется. Он представился именем Валакас.")
                    write_msg(event.user_id, event.random_id, "Валакас сказал что за ним следят люди старейшины, он дал"
                                                              " вам камень для ритуала на сохранность. Если с ним что-то"
                                                              " случится, вам непоздоровится.")
                    nekr+=1
                    stone +=1
                    vyborizkladbishe()
                elif request == "поговорить" and nekr==1:
                    write_msg(event.user_id, event.random_id, "Валакас ждет не дождется когда вы приведете какого-нибудь "
                                                              "ребенка. Все его мечты только о ритуале. С каждой минутой он"
                                                              " становится безумнее.")
                    nekr += 1
                elif request == "поговорить" and nekr==2:
                    write_msg(event.user_id, event.random_id, "Валакас: Какого черта ты еще не привел ребенка? Времени сов"
                                                              "сем не осталось! Если ты сегодня до полуночи не найдешь дитя"
                                                              ", жертвой в ритуале станешь ты!")
                    nekr += 1
                elif request == "поговорить" and nekr == 3:
                    write_msg(event.user_id, event.random_id, "Валакас совсем обезумел. Он бросил вам под ноги какую-то склянку"
                                                              ", после того как она разбилась - поднялся дым. Вы начали терять "
                                                              "сознание...")
                    write_msg(event.user_id, event.random_id, "Когда вы очнулись - обнаружили что вы прикованы цепями к плите."
                                                              " Вам никак не выбраться отсюда. Через несколько минут Валакас"
                                                              " подошел к вам, вы увидили как его ритуальный кинжал отражает "
                                                              "свет луны. Он начал говорить на непонятном языке и вонзил кинжал"
                                                              " вам в сердце. Последнее что вы увидели перед смертью - это безу"
                                                              "мную улыбку Валакаса и яркую полную луну...")
                    write_msg(event.user_id, event.random_id, " /GAME OVER\ ")
                    write_msg(event.user_id, event.random_id, "Чтобы начать квест заного, напишите (начать).")
                    start()
                elif request == "ритуал" and girl ==0 and nekr ==0:
                    write_msg(event.user_id, event.random_id, "Какой ритуал, вы о чем?")
                elif request == "ритуал" and girl ==0 and nekr >0:
                    write_msg(event.user_id, event.random_id, "Без невинного дитя ритуал не сработает.")
                elif request == "ритуал" and girl ==1:
                    write_msg(event.user_id, event.random_id, "В полночь вы с Валакасом совершили ритуал призыва нечисти."
                                                              " Из могил начали вставать зомби, они сбились в толпу и сно"
                                                              "сили все на своем пути. Вы не успели сбежать так как нахо"
                                                              "дились в самом центре кладбища. Вас и Валакаса разорвали "
                                                              "на тысячи мелких кусочков. Огромная толпа зомби двинулась "
                                                              "в сторону деревни.")
                    write_msg(event.user_id, event.random_id, " /GAME OVER\ ")
                    girl -=1
                    write_msg(event.user_id, event.random_id, "Чтобы начать квест заного, напишите (начать).")
                    start()

                else:
                    write_msg(event.user_id, event.random_id, "Доступные действия (поговорить), (осмотреться), (центр), (ритуал).")